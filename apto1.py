import numpy as np
import time

import element
import preprocess
import solver
import postprocess
import problem

# import matplotlib.pyplot as plt

# num_elements = [6, 8, 10, 16, 20, 26, 32, 40, 80, 100]

num_elements = [8, 10, 16, 20, 30, 50, 80, 100, 256, 512, 1024, 1500]

omega_n_evol = []
mode_n_evol = []
times = []

n_freq = "all"
# n_freq = "5"

for n in num_elements:
    print(n)
    tic = time.time()
    # n_freq = n

    elements = preprocess.mesh(funProblem=problem.problem, n=n, method="equi")

    K = solver.assembler(elements, matrix="K")
    Mc = solver.assembler(elements, matrix="Mc")

    # quitar primer nodo ya que no gira (eliminar modo de solido rigido)
    K_1 = K[1:, 1:]
    Mc_1 = Mc[1:, 1:]

    # calculo de frecuencias y modos propios
    if n_freq == "all":
        omega_n, mode_n = postprocess.natural_frequencies(K_1, Mc_1, num_nfreq=n)
        o_n = omega_n[:]
        o_n.sort()
        o_n = omega_n[:5]
        omega_n_evol.append(o_n)
        # mode_n_evol.append(mode_n)
    elif n_freq == "5":
        omega_n, mode_n = postprocess.natural_frequencies(K_1, Mc_1, num_nfreq=5)
        o_n = omega_n[:]
        o_n.sort()
        omega_n_evol.append(o_n)
        # mode_n_evol.append(mode_n)
    toc = time.time()
    times.append(toc-tic)

print("writing solution...")

sol_val = np.zeros((len(num_elements), 7))
# sol_vec = np.zeros((len(num_elements), 6))

sol_val[:,0] = np.asarray(num_elements).T
sol_val[:,1:6] = np.asarray(omega_n_evol)
sol_val[:,6] = np.asarray(times).T

np.savetxt("omega_n.txt", sol_val, delimiter=',', 
           header='Number of element , omega_1 , omega_2 , omega_3 , omega_4 , omega_5 , Time of computation')

# # plotting
# postprocess.plot_as_latex()

# plt.figure()
# plt.plot(num_nodes, omega_n_evol[0])
# plt.show()
