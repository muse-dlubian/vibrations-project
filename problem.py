"""
Problem

Functions for the problem to resolve
"""

import numpy as np


def problem(node_i, node_j):
    """
    Given the initial and final position of the two nodes that define an element, returns the I and GJ parameters for that position

    Parameters:
    ----------
    * `node_i`: 2-element tuple that contains the 2-D position in global axes for the node i
    * `node_j`: 2-element tuple that contains the 2-D position in global axes for the node j

    Returns:
    -------
    * `res_I`: I in the element between `node_i` and `node_j`
    * `res_GJ`: GJ in the element between `node_i` and `node_j`
    """
    L = 1.
    I = 1.
    GJ = 1.
    I_D = I * L

    bar_1 = [(0., 0.), (0.5 * L, 0.)]
    GJ_bar_1 = 2. * GJ
    I_bar_1 = 2. * I

    bar_2 = [(0.5 * L,  0.), (1. * L, 0.)]
    GJ_bar_2 = 1. * GJ
    I_bar_2 = 1. * I

    disk_1 = [(0.5 * L, 0.)]
    I_disk_1 = 2. * I_D

    disk_2 = [(1.0 * L, 0.)]
    I_disk_2 = 1. * I_D

    tol = 1e-6

    # bar 1
    if (node_i[0] >= bar_1[0][0] and np.abs(node_i[1] - bar_1[0][1]) < tol) and (node_j[0] <= bar_1[1][0] and np.abs(node_j[1] - bar_1[1][1]) < tol):
        res_I = I_bar_1
        res_GJ = GJ_bar_1
        # disk 1
        if np.abs(node_j[0] - disk_1[0][0]) < tol:
            res_I += I_disk_1
            # print("in 1")
        return res_I, res_GJ
    # bar 2
    elif (node_i[0] >= bar_2[0][0] and np.abs(node_i[1] - bar_2[0][1]) < tol) and (node_j[0] <= bar_2[1][0] and np.abs(node_j[1] - bar_2[1][1]) < tol):
        res_I = I_bar_2
        res_GJ = GJ_bar_2
        # disk 2
        if np.abs(node_j[0] - disk_2[0][0]) < tol:
            res_I += I_disk_2
            # print("in 2")
        return res_I, res_GJ
    else:
        print("problem: not in bar")
        return -1, -1


def load_armonic_cos(t, amp, omega, phase):
    """
    Armonic function:

    $M(t) = A \cos(\Omega t + \phi)$

    Parameters:
    ----------
    * `t`: time, in s, $t$
    * `amp`: amplitude, $A$
    * `omega`: frequency, in rad/s, $\Omega$
    * `phase`: phase, in rad, $\phi$

    Returns:
    -------
    * `value`: $M$
    """
    return amp * np.cos(omega * t + phase)


def loads_armonic_vector(t, ne, amp, omega, phase):
    """
    Armonic function:

    $M(t) = A \cos(\Omega t + \phi)$

    Parameters:
    ----------
    * `t`: time, in s, $t$
    * `ne`: number of elements
    * `amp`: amplitude, $A$
    * `omega`: frequency, in rad/s, $\Omega$
    * `phase`: phase, in rad, $\phi$

    Returns:
    -------
    * `loads`: vector with the armonic load applied in the last element (where disk 2 is)
    """
    loads = np.zeros((ne + 1,))
    loads[-1] = load_armonic_cos(t, amp, omega, phase)

    return loads


def load_whiteNoise(t, range_):
    """
    White noise load

    Parameters:
    ----------
    * `t`: time, in s, $t$
    * `range_`: range of the white noise

    Returns:
    -------
    * `alea`: random number in range `range_`, from a uniform distribution
    """
    r0 = range_[0]
    r1 = range_[1]

    alea = np.random.random()
    alea *= (r1 - r0)
    alea += r0
    return alea


def loads_whiteNoise_vector(t, ne, range_):
    """
    White noise load

    Parameters:
    ----------
    * `t`: time, in s, $t$
    * `ne`: number of elements
    * `range_`: range of the white noise

    Returns:
    -------
    * `loads`: vector with the white noise load applied in the middle element (where disk 1 is)
    """
    loads = np.zeros((ne + 1, ))
    loads[int((ne + 1) / 2)] = load_whiteNoise(t, range_)

    return loads
