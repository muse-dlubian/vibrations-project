# MUSE 2017 - 2018: Trabajo de Vibraciones

*Autores*

* Aguilar Redondo, Christian
* Llobera Ferriol, Miquel
* Lubi�n Arenillas, Daniel

El programa se compone de los siguientes m�dulos:

* `element`: contiene la clase `Element`, donde est�n contenidas las propiedades del elemento a torsi�n.
* `preprocess`: mallado.
* `solver`: ensamblado de matrices, integraci�n num�rica de la ecuaci�n de Lagrange de peque�as perturbaciones.
* `postprocess`: graficado, an�lisis de matrices