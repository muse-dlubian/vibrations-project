"""
Module that contains the class `Element`
"""

import numpy as np
import scipy.linalg as la


class Element:
    """Class definition of `Element`, designed to contain all the
        appropiate information regarding torsion in FEM
    """

    def __init__(self, funProblem, i, j, node_i, node_j):

        self.i = i
        self.j = j
        self.node_i = np.asarray(node_i)
        self.node_j = np.asarray(node_j)

        self.L = la.norm(self.node_j - self.node_i)

        I, GJ = funProblem(self.node_i, self.node_j)
        self.GJ = GJ
        self.I = I

        self.K = (self.GJ / self.L) * np.asarray([[1., -1.], [-1., 1.]])
        self.Mc = (self.I * self.L / 6) * np.asarray([[2.,  1.], [1., 2.]])
        self.Mp = (self.I * self.L / 2) * np.asarray([[1.,  0.], [0., 1.]])

        self.th = np.arctan2(self.node_j[1] - self.node_i[1],
                             self.node_j[0] - self.node_i[0])

        self.loc = np.asarray([self.i, self.j])

        self.T = np.asarray(
            [[np.cos(self.th), np.sin(self.th)], [-np.sin(self.th), np.cos(self.th)]])

        self.K_glob = np.dot(np.dot(self.T.T, self.K), self.T)
        self.Mc_glob = np.dot(np.dot(self.T.T, self.Mc), self.T)
        self.Mp_glob = np.dot(np.dot(self.T.T, self.Mp), self.T)

    def __str__(self):
        return "Element between nodes %i=%s and %i=%s" % (self.i, str(self.node_i), self.j, str(self.node_j))

    def get_matrix_global(self, matrix):
        """
        Method that returns the corresponding matrix

        Parameters
        ---

        * `matrix`: "K", "Mc", "Mp"

        Returns
        ---
        `K_glob`, `Mc_glob`, `Mp_glob`
        """
        # TODO: pasar a sistema try: except: para los errores
        if matrix == "K":
            return self.K_glob
        elif matrix == "Mc":
            return self.Mc_glob
        elif matrix == "Mp":
            return self.Mp_glob
        else:
            print("In get_matrix_global: matrix " + matrix + "not found")

    def get_loc_vector(self):
        """
        Returns location vector
        """
        return self.loc
