import numpy as np
import scipy.linalg as la

import element
import preprocess
import solver
import postprocess

import problem

# number of elements
n = 512

elements = preprocess.mesh(funProblem=problem.problem, n=n, method="equi")

K = solver.assembler(elements, matrix="K")
Mc = solver.assembler(elements, matrix="Mc")
# Mp = solver.assembler(elements, matrix="Mp")

# quitar primer nodo ya que no gira (eliminar modo de solido rigido)
K_1 = K[1:, 1:]
Mc_1 = Mc[1:, 1:]
# Mp_1 = Mp[1:, 1:]

omega_n_all, mode_n_all = postprocess.natural_frequencies(
    K_1, Mc_1, num_nfreq=K_1.shape[0])
omega_n, mode_n = postprocess.natural_frequencies(
    K_1, Mc_1, num_nfreq=10)
# print("frecuencias propias: ", omega_n[-5:])
print("frecuencias propias: ", omega_n)
print(mode_n.shape)
# print("modos propios: ", mode_n)

omega_sorted = omega_n[:]
omega_sorted.sort()


# APTO 3
print("\n APTO 3\n")

Omegas = [0.9 * omega_sorted[0], 1.2 * omega_sorted[1]]
print("omegas: ", Omegas)

t = np.linspace(0, 10, int(500))
q_disk1_pArm = np.zeros((t.shape[0], 3))
q_disk1_pArm[:, 0] = t[:]

# for oi in range(0, len(Omegas)):
#     print(oi)
#     H = postprocess.transfer_matrix(
#         Omegas[oi], K_1, Mc_1, omega_n, mode_n, 3, 10)
#     for ti in range(0, t.shape[0]):
#         p = problem.load_armonic_cos(
#             t=t[ti], amp=1., omega=Omegas[oi], phase=0.)
#         q_disk1_pArm[ti, oi + 1] = H[(H.shape[0] - 1) // 2, H.shape[0] - 1] * p

for oi in range(0, len(Omegas)):
    # print(oi)
    H_jk = postprocess.transfer_matrix_element(
        Omegas[oi], K_1, Mc_1, omega_n, mode_n, 3, 10, (K_1.shape[0] - 1) // 2, K_1.shape[0] - 1)
    for ti in range(0, t.shape[0]):
        print(ti)
        p = problem.load_armonic_cos(
            t=t[ti], amp=1., omega=Omegas[oi], phase=0.)
        q_disk1_pArm[ti, oi + 1] = H_jk * p



np.savetxt("apto3.txt", q_disk1_pArm, delimiter=' , ',
           header="Tiempo , 0.9 omega_1 , 1.2 omega_2")

# APTO 4
print("\n APTO 4\n")


def psd_disk2(omega):
    if omega >= 0.5 * omega_sorted[0] and omega <= 1.5 * omega_sorted[2]:
        return 1.
    else:
        return 0.


range_omega = np.linspace(1e-7, omega_sorted[4], int(300))
q_disk2_pRan = np.empty((range_omega.shape[0], 2))
q_disk2_pRan[:, 0] = range_omega[:]

print("range_omega: ", range_omega[0], "...", range_omega[-1])

i = 0
for o in range_omega:
    print(i)
    check_if_near_omega_n = np.asarray([np.isclose(omega_sorted[0], o),
                        np.isclose(omega_sorted[1], o),
                        np.isclose(omega_sorted[2], o),
                        np.isclose(omega_sorted[3], o),
                        np.isclose(omega_sorted[4], o)], dtype=bool)

    if check_if_near_omega_n.any():
        q_disk2_pRan[i, 1] = 0.
    else:
        if np.isclose(psd_disk2(o), 0.):
            q_disk2_pRan[i, 1] = 0.
        else:
            # H = postprocess.transfer_matrix(
            #     o, K_1, Mc_1, omega_n, mode_n, 5, 5)
            # q_disk2_pRan[i, 1] = H[H.shape[0] - 1,
            #                        (H.shape[0] - 1) // 2] * psd_disk2(o)
            H_jk = postprocess.transfer_matrix_element(
                o, K_1, Mc_1, omega_n, mode_n, 5, 5, K_1.shape[0] - 1, (K_1.shape[0] - 1) // 2)
            q_disk2_pRan[i, 1] = H_jk * psd_disk2(o)
    i += 1

np.savetxt("apto4.txt", q_disk2_pRan, delimiter=' , ',
           header="Omega , psd_disk2")
