import numpy as np
import scipy.integrate as scint
import scipy.linalg as la


def assembler(elements, matrix):
    """
    Assembles the given matrix `matrix` and returns it.

    Parameters:
    ----------
    * `elements`: array of elements
    * `matrix`: string with the name of the matrix. 
                See definition of the method `get_matrix_global` 
                in class `Element` in module `element.py`

    Returns:
    -------
    * `A`: assembled matrix

    """
    A = np.zeros((len(elements) + 1, len(elements) + 1))

    for element in elements:
        mat = element.get_matrix_global(matrix)
        loc = element.get_loc_vector()

        for i in range(0, mat.shape[0]):
            for j in range(0, mat.shape[1]):
                A[loc[i], loc[j]] += mat[i, j]
    return A


def Lagrange_equation_Right_Member(t, x, M, K, P):
    """
    Right member of Lagrange equation for a typical vibrations problem
    without damping.

    Parameters:
    ---------
    * `t`: time
    * `x`: state vector (displacements and velocities)
    * `M`: matrix of masses
    * `K`: matrix of stiffness
    * `P`: vector of loads

    Returns:
    -------
    * `x_dot`: first derivative of x
    """

    n = x.shape[0] // 2

    x1 = x[:n]
    x2 = x[n:]

    x1_dot = x2
    x2_dot = la.inv(M) @ (P - K @ x1)

    x_dot = np.zeros_like(x)
    x_dot[:n] = x1_dot[:]
    x_dot[n:] = x2_dot[:]
    return x_dot


def integrator(file_sol,
               q,
               tf,
               K,
               M,
               loads_fun,
               loads_fun_args=(),
               t0=0.,
               dt=1e-2):
    """
    Temporal integrator (UNDER DEVELOPMENT)
    """

    ne = q.shape[0] - 1
    y = np.append(q, q, axis=0)
    P = loads_fun(t0, ne, *loads_fun_args)

    eq = scint.ode(Lagrange_equation_Right_Member)
    eq.set_initial_value(y, t0)
    eq.set_integrator("dopri45")
    eq.set_f_params(M, K, P)

    with open(file_sol, "w+") as F:
        i = 0

        while eq.successful() and eq.t < tf:

            eq.integrate(eq.t)
            y = eq.y
            t = eq.t + dt

            eq.set_initial_value(y, t)
            P = loads_fun(t, ne, *loads_fun_args)
            # print(P)
            eq.set_f_params(M, K, P)
            i += 1
            if np.abs(eq.t - t0 + i * dt) < 1e-5:
                print("problema con tiempos de integracion")

            # print([q for q in (y.T).tolist()[0][:]])
            F.write(str(x2q(y).T.tolist()[0][:]) + "\n")
        F.close()


def x2q(x):
    """
    Takes `x` and returns the first half of the vector
    """

    n = x.shape[0] // 2
    q = x[:n]
    return q


def x2qq(x):
    """
    Takes `x` and returns the two halves of the vector
    """

    n = x.shape[0] // 2
    q1 = x[:n]
    q2 = x[n:]
    return q1, q2


def q2x(q, q_dot):
    """
    Takes `q` and `q_dot`, puts them together in `x` and returns `x`
    """
    x = np.empty((2 * q.shape[0],))
    x[:n] = q[:]
    x[n:] = q_dot[:]
    return x
