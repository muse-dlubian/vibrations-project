import numpy as np
from element import Element


def mesh(funProblem, n, method="equi"):
    """
    Meshing function

    Parameters:
    ----------
    * `funProblem`(node_i, node_j) : defines the geometry and the problem to solve. Should have as arguments
                    node_i and node_j
    * `n`: number of elements
    * `method`: if equispacing ("equi", default) or other, as a Chevyshev-Gauss-Radau ("cgr")

    Returns:
    -------
    * `elements`: list of initialized Element
    """
    L = 1.  # TODO: meter en otro lado. lo ideal seria que fuera entrada
    nodes = np.zeros((n + 1, 2))
    if method == "equi":
        nodes[:, 0] = np.linspace(0, L, num=n + 1)
    # elif method == "CGR"
        # nodes[:,0] =
    # TODO: add chevyshev-gauss-radau o semejante (no equiespaciada)

    elements = []  # TODO: darle un tamanho inicial
    for ni in range(0, n):  # from 0 to n-1
        # print(ni)
        elements.append(Element(funProblem=funProblem,
                                i=ni,
                                j=ni + 1,
                                node_i=nodes[ni, :],
                                node_j=nodes[ni + 1, :]))
    return elements
