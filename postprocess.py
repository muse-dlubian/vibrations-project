"""
Postprocessing rutines:

* Analysis of data
* Plotting
"""

import matplotlib
import matplotlib.pylab as plt

import numpy as np
import scipy.linalg as la


def plot_as_latex(font="lmodern", size=18, ext=".png", dpi=150):
    """Gives format to the plots in the same
    "enviroment" to have Latin Modern Roman
    or Palatino.

    Parameters
    ---
    font: selected font, between Latin Modern Roman ("lmodern")
            or Palatino ("palatino")
    size: font size
    ext:    extension for savefig
    dpi:    dpi for savefig

    Returns
    ---
    ext:    extension for savefig
    dpi:    dpi for savefig
    """
    import matplotlib

    # fontName = ['Latin Modern Roman' if font == "lmodern"]
    # fontName = ['Palatino' if font == "palatino"]
    fontName = 'Latin Modern Roman'

    # Formateo graficas:
    matplotlib.rcParams['text.latex.unicode'] = True
    plt.rc('font', **{'family': 'serif', 'serif': [fontName]})
    # plt.rc('font', **{'family': 'serif', 'serif': ['Latin Modern Roman']})
    # plt.rc('font', **{'family':'serif', 'serif':['Palatino']})
    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath}' +
           r'\usepackage[spanish]{babel}\usepackage{siunitx}')
    matplotlib.rcParams.update({'font.size': size})
    extFig = ext
    # extFig = ".png"
    # extFig = ".pdf"
    # extFig = ".eps"
    # dpinum = 200
    dpinum = dpi
    return extFig, dpinum


def plot(filename):
    extFig, dpinum = plot_as_latex()
    # TODO: todo
    pass


def natural_frequencies(K, M, num_nfreq):  # , num_nfreq=K.shape[0]): # FIXME:
    # def natural_frequencies(K, M, num_nfreq):  # , num_nfreq=K.shape[0]): # FIXME:
    """ Function that return the num_nfreq lowest 
    natural frequencies and the eigenmodes of the
    system with K and M matrices

    Parameters
    ---
    K:  K matrix
    M:   M matrix
    num_nfreq:  [Optional] number of natural frequencies
                to calculate, starting from the smallest 
                ones. If not set, all are returned.

    Returns
    ---
    omega:  ndarray containing the natural frequencies,
            sorted from smallest to largest
    modes:  the correspondent eigenmodes

    """
    import numpy as np
    import scipy.linalg as la
    import scipy.sparse.linalg as sla

    if num_nfreq == K.shape[0]:
        # print("Computing natural frequencies...\n\tUsing scipy.linalg.eig...")
        # omega2, modes = la.eig(K.dot(la.inv(M)))
        omega2, modes = la.eig(K, M)

    # TODO: mirar las funciones para matrices tridiagonales
    # quizas K M_inv sea tb tridiagonal. No lo es: sale pentadiagonal
    # te permite tambien calcular un numero determinado solo
    elif num_nfreq < K.shape[0] - 1:
        #     # print("Computing natural frequencies....\n\tUsing scipy.sparse.linalg.eigs...")
        # omega2, modes = sla.eigs(K.dot(la.inv(M)),
                                #  k=num_nfreq,
                                #  which='SM')
        omega2, modes = sla.eigs(K,
                                 k=num_nfreq,
                                 M=M,
                                 which='SM')
    #     # TODO: mirar argumento M de la funcion, es posible que sea
    #     # el correspondiente a utilizar la matriz de rigidez K como
    #     # ortogonalizador

    #     # TODO: mirar si se puede mejorar el rendimiento usando el
    #     # which='LM' para Shift-Invert Mode
    #     # https://docs.scipy.org/doc/scipy/reference/tutorial/arpack.html#shift-invert-mode
    else:
        raise ValueError(
            "Argument `num_nfreq` not valid. Should be `num_nfreq < K.shape[0]-1` or `num_nfreq == K.shape[0]`")

    omega = np.sqrt(omega2)
    # omega.sort()
    # FIXME: reordenar los modos de la forma correspondiente
    return np.real_if_close(omega), np.real_if_close(modes)


def transfer_matrix(Omega, K, M, nat_freq, modes, g1, g2):
    """
    Computes the transfer matrix

    Parameters:
    ----------
    * `Omega`: frequency of computation, in radians/s
    * `K`: matrix of 
    * `M`: 
    * `nat_freq`: vector with the natural frequencies in radians/s
    * `modes`: matrix that contains the eigenmodes of the system
    * `g1`: number of modes to aproximate `term1`
    * `g2`: number of modes to aproximate `term2`

    Returns:
    -------
    * `H`: transfer matrix
    """

    H = np.zeros_like(K)

    # M_r_diag = np.diagonal(modes.T @ M @ modes)
    # K_inv = la.inv(K)

    phi = modes
    w = nat_freq

    for j in range(0, H.shape[0]):
        for k in range(0, H.shape[1]):
            term1 = 0.
            for r in range(0, g1):
                M_r = phi[:, r].T @ M @ phi[:, r]
                term1 += (phi[j, r] * phi[k, r]) / (M_r  # TODO: comprobar phi[j,r]
                                                    * (w[r]**2)) / (1 - (Omega / w[r])**2)
            # term2 = K_inv[j, k]
            term2 = 0.
            # for r in range(0, g2):
            for r in range(g1, g2):
                M_r = phi[:, r].T @ M @ phi[:, r]
                # term2 -= (phi[j, r] * phi[k, r]) / (M_r * (w[r]**2))
                term2 += (phi[j, r] * phi[k, r]) / (M_r * (w[r]**2))
            H[j, k] = term1 + term2
    return H


def transfer_matrix_element(Omega, K, M, nat_freq, modes, g1, g2, j, k):
    """
    Computes the term [`j`,`k`] of the transfer matrix

    Parameters:
    ----------
    * `Omega`: frequency of computation, in radians/s
    * `K`: matrix of 
    * `M`: 
    * `nat_freq`: vector with the natural frequencies in radians/s
    * `modes`: matrix that contains the eigenmodes of the system
    * `g1`: number of modes to aproximate `term1`
    * `g2`: number of modes to aproximate `term2`
    * `j`: row j of the transfer matrix
    * `k`: column k of the transfer matrix

    Returns:
    -------
    * `H`: term [`j`,`k`] of the transfer matrix
    """
    # K_inv = la.inv(K)

    phi = modes
    w = nat_freq

    term1 = 0.
    for r in range(0, g1):
        M_r = phi[:, r].T @ M @ phi[:, r]
        term1 += (phi[j, r] * phi[k, r]) / (M_r  # TODO: comprobar phi[j,r]
                                            * (w[r]**2)) / (1 - (Omega / w[r])**2)
    # term2 = K_inv[j, k]
    term2 = 0.
    # for r in range(0, g2):
    for r in range(g1, g2):
        M_r = phi[:, r].T @ M @ phi[:, r]
        # term2 -= (phi[j, r] * phi[k, r]) / (M_r * (w[r]**2))
        term2 += (phi[j, r] * phi[k, r]) / (M_r * (w[r]**2))

    H = term1 + term2

    return H
